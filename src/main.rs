use std::collections::HashSet;
use std::error::Error;

struct Rope {
    head: (i32, i32),
    tail: (i32, i32),
    tail_positions: HashSet<(i32, i32)>,
}

impl Rope {
    fn new() -> Self {
        let mut tail_positions = HashSet::new();
        tail_positions.insert((0, 0));

        Self {
            head: (0, 0),
            tail: (0, 0),
            tail_positions,
        }
    }

    fn dx(&self) -> i32 {
        self.head.0 - self.tail.0
    }

    fn dy(&self) -> i32 {
        self.head.1 - self.tail.1
    }

    fn get_length(&self) -> i32 {
        let length = (self.dx() as f32).hypot(self.dy() as f32).abs();
        length as i32
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
struct Knot {
    x: i32,
    y: i32,
}

impl Knot {
    fn dx(&self, other: &Knot) -> i32 {
        other.x - self.x
    }

    fn dy(&self, other: &Knot) -> i32 {
        other.y - self.y
    }

    fn distance_to(&self, other: &Knot) -> i32 {
        let length = (self.dx(other) as f32).hypot(self.dy(other) as f32).abs();
        length as i32
    }
}

struct LongRope {
    knots: Vec<Knot>,
    tail_positions: HashSet<(i32, i32)>,
}

impl LongRope {
    fn new(length: usize) -> Self {
        let mut tail_positions = HashSet::new();
        tail_positions.insert((0, 0));

        LongRope {
            knots: vec![Knot { x: 0, y: 0 }; length],
            tail_positions,
        }
    }
    fn record_tail_pos(&mut self) -> Result<bool, Box<dyn Error>> {
        let tail = self.knots.last().ok_or("can't find the tail!")?;
        Ok(self.tail_positions.insert((tail.x, tail.y)))
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_a(input)?);
    println!("Solution B: {}", solve_b(input)?);
    Ok(())
}

fn solve_a(input: &str) -> Result<usize, Box<dyn Error>> {
    let mut rope = Rope::new();
    for mv in input.lines() {
        let vector = mv.split_whitespace().collect::<Vec<&str>>();
        let direction = vector.first().ok_or("can't read the vector direction")?;
        let distance = vector
            .get(1)
            .ok_or("can't read the vector distance")?
            .parse::<i32>()?;

        for _ in 0..distance {
            match *direction {
                "U" => rope.head.1 += 1,
                "R" => rope.head.0 += 1,
                "D" => rope.head.1 -= 1,
                "L" => rope.head.0 -= 1,
                _ => {}
            }

            if rope.get_length() > 1 {
                if (rope.dx() * rope.dy()) != 0 {
                    rope.tail.0 += if rope.dx().is_positive() { 1 } else { -1 };
                    rope.tail.1 += if rope.dy().is_positive() { 1 } else { -1 };
                } else if rope.dx() != 0 {
                    rope.tail.0 += if rope.dx().is_positive() { 1 } else { -1 };
                } else {
                    rope.tail.1 += if rope.dy().is_positive() { 1 } else { -1 };
                }

                rope.tail_positions.insert((rope.tail.0, rope.tail.1));
            }
        }
    }

    Ok(rope.tail_positions.len())
}

fn solve_b(input: &str) -> Result<usize, Box<dyn Error>> {
    const LENGTH: usize = 10;
    let mut rope = LongRope::new(LENGTH);

    for mv in input.lines() {
        let vector = mv.split_whitespace().collect::<Vec<&str>>();
        let direction = vector.first().ok_or("can't read the vector direction")?;
        let distance = vector
            .get(1)
            .ok_or("can't read the vector distance")?
            .parse::<i32>()?;

        for _ in 0..distance {
            let mut head = rope
                .knots
                .first_mut()
                .ok_or("can't find the head of the rope!")?;

            match *direction {
                "U" => head.y += 1,
                "R" => head.x += 1,
                "D" => head.y -= 1,
                "L" => head.x -= 1,
                _ => {}
            }

            for idx in 1..LENGTH {
                let prev = rope
                    .knots
                    .get(idx - 1)
                    .cloned()
                    .ok_or("can't find the previous knot!")?;

                let knot = rope.knots.get_mut(idx).ok_or("can't find the knot")?;
                if knot.distance_to(&prev) > 1 {
                    let dx = knot.dx(&prev);
                    let dy = knot.dy(&prev);

                    if (dx * dy) != 0 {
                        knot.x += if dx.is_positive() { 1 } else { -1 };
                        knot.y += if dy.is_positive() { 1 } else { -1 };
                    } else if dx != 0 {
                        knot.x += if dx.is_positive() { 1 } else { -1 };
                    } else {
                        knot.y += if dy.is_positive() { 1 } else { -1 };
                    }
                }
            }

            rope.record_tail_pos()?;
        }
    }

    Ok(rope.tail_positions.len())
}

#[test]
fn test_solve_a() {
    let input = include_str!("../test.txt");
    assert_eq!(13, solve_a(input).unwrap());
}

#[test]
fn test_solve_b() {
    let input = include_str!("../test.txt");
    assert_eq!(1, solve_b(input).unwrap());

    let input = include_str!("../test2.txt");
    assert_eq!(36, solve_b(input).unwrap());
}
